# Prepare

### 1. Download

- Golang: [Click Here](https://go.dev/dl/)
- XAMPP: [Click Here](https://www.apachefriends.org/download.html)
- Postman: [Click Here](https://www.postman.com/downloads/?utm_source=postman-home)

### 2. Install

- Instruksi lengkap ada di  [Golang Official Website](https://go.dev/doc/install)

- Cara cek versi google di Terminal atau Command Propt:

  ```bash
  go version
  ```

### 3. Extension on VS Code

![Go Extension](./extension.png)
